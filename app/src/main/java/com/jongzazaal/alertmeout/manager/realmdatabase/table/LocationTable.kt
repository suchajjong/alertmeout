package com.jongzazaal.alertmeout.manager.realmdatabase.table

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.Required

open class LocationTable(
    @PrimaryKey()
    var id: Long = 0L,

    @Required
    var name_place:String = "",

    @Required
    var latitude:String = "",

    @Required
    var longitude:String = ""
): RealmObject() {
}