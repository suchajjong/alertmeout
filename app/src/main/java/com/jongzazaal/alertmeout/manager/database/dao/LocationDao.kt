package com.jongzazaal.alertmeout.manager.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.jongzazaal.alertmeout.manager.database.table.LocationTable

@Dao
interface LocationDao {

    @Insert
    fun insert(location:LocationTable)

    @Update
    fun update(location:LocationTable)

    @Query("SELECT * from location WHERE id = :key ")
    fun get(key:Long):LocationTable?

    @Query("DELETE FROM location")
    fun clear()

//    @Query("SELECT * FROM daily_sleep_quality_table ORDER BY nightId DESC LIMIT 1")
//    fun getTonight(): SleepNight?

    @Query("SELECT * FROM location ORDER BY id DESC")
    fun getAllLocation(): LiveData<List<LocationTable>>
}