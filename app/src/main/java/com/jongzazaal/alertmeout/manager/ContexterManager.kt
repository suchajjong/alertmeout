package com.jongzazaal.alertmeout.manager

import android.annotation.SuppressLint
import android.content.Context

class ContexterManager private constructor() {

    private lateinit var context: Context

    companion object {
        @SuppressLint("StaticFieldLeak")
        private val contextInstance =
            ContexterManager()

        fun getInstance(): ContexterManager {
            return contextInstance
        }
    }

    fun setApplicationContext(context: Context) {
        this.context = context
    }

    fun getApplicationContext(): Context {
        return context
    }

}