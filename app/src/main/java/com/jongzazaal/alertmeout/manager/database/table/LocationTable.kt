package com.jongzazaal.alertmeout.manager.database.table

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "location")
data class LocationTable(
    @PrimaryKey(autoGenerate = true)
    val id: Long = 0L,

    @ColumnInfo(name = "name_place")
    val namePlace:String = "",

    @ColumnInfo(name = "latitude")
    val latitude:String = "",

    @ColumnInfo(name = "longitude")
    val longitude:String = ""
)