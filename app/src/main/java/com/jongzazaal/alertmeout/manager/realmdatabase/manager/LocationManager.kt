package com.jongzazaal.alertmeout.manager.realmdatabase.manager

import com.jongzazaal.alertmeout.manager.realmdatabase.table.LocationTable
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmResults

class LocationManager {
    private lateinit var realm: Realm
    constructor(){
        initRealm()
    }
    private fun initRealm(){
        val config = RealmConfiguration.Builder()
            .name("myrealm.realm")
//            .encryptionKey(getMyKey())
            .schemaVersion(1)
//            .modules(MySchemaModule())
//            .migration(MyMigration())
            .build()
// Use the config
        realm = Realm.getInstance(config)
    }

    fun clearAll(){
        realm.beginTransaction()
        realm.delete(LocationTable::class.java)
        realm.commitTransaction()
    }
    fun deleteLocation(id: Int) {
        realm.beginTransaction()
        realm.where(LocationTable::class.java)
            .equalTo("id", id)
            .findFirst()
            ?.deleteFromRealm()
        realm.commitTransaction()
    }
    fun addLocation(item: LocationTable){
        val currentId = realm.where(LocationTable::class.java).max("id")
        item.id = (currentId?:0).toLong()+1
//        realm.beginTransaction()
//        realm.insertOrUpdate(item)
//        realm.commitTransaction()

        realm.executeTransaction { realm ->
            realm.insertOrUpdate(item)
        }
    }
    fun getLocation(id: Long): RealmResults<LocationTable>{
        return realm.where(LocationTable::class.java).equalTo("id", id).findAll()
    }
    fun getLocationAll(): RealmResults<LocationTable>{
        return realm.where(LocationTable::class.java).findAll()
    }

    companion object{
        private var instance: LocationManager? = null
        fun getInstance(): LocationManager{
            return instance?:LocationManager()
        }
    }
}