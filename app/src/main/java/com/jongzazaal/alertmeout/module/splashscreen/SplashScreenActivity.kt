package com.jongzazaal.alertmeout.module.splashscreen

import android.os.Bundle
import android.os.Handler
import com.jongzazaal.alertmeout.R
import com.jongzazaal.alertmeout.base.BaseActivity
import com.jongzazaal.alertmeout.databinding.ActivitySplashScreenBinding
import com.jongzazaal.alertmeout.module.main.MainActivity

class SplashScreenActivity : BaseActivity() {
    private val binding: ActivitySplashScreenBinding by lazy { ActivitySplashScreenBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val handler = Handler()
        handler.postDelayed(Runnable { // Do something after 5s = 5000ms
            MainActivity.open(this)
        }, 3000)
    }

    override fun initListener() {

    }
}
