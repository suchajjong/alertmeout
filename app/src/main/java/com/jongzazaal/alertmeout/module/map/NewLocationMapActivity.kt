package com.jongzazaal.alertmeout.module.map

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.jongzazaal.alertmeout.R
import com.jongzazaal.alertmeout.base.BaseActivity
import com.jongzazaal.alertmeout.databinding.ActivityNewLocationMapBinding


class NewLocationMapActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private val binding: ActivityNewLocationMapBinding by lazy { ActivityNewLocationMapBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun initListener() {

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
//        val sydney = LatLng(-34.0, 151.0)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
        mMap.setOnMapLongClickListener {
            mMap.clear()
            mMap.addMarker(MarkerOptions().position(it).title("Marker in Select"))
            mMap.addCircle(
                CircleOptions()
                    .center(it)
                    .radius(10000.0)
                    .strokeColor(Color.RED)
                    .fillColor(Color.BLUE)
            )
        }
    }
    companion object{

//        private const val PAGE_ID         = "page_id"
//        private const val OPEN_REGISTER   = "open_register"
//        const val PAGE_HOME       = R.id.menu_home

        fun open(context: Context) {
            val intent = Intent(context, NewLocationMapActivity::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//            intent.putExtra(PAGE_ID, position)
//            intent.putExtra(OPEN_REGISTER, false)
            ActivityCompat.startActivity(
                context, intent, null
            )
        }
    }
}
