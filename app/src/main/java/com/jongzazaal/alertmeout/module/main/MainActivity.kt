package com.jongzazaal.alertmeout.module.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.jongzazaal.alertmeout.R
import com.jongzazaal.alertmeout.base.BaseActivity
import com.jongzazaal.alertmeout.databinding.ActivityMainBinding
import com.jongzazaal.alertmeout.manager.realmdatabase.manager.LocationManager
import com.jongzazaal.alertmeout.manager.realmdatabase.table.LocationTable
import com.jongzazaal.alertmeout.module.map.NewLocationMapActivity

class MainActivity: BaseActivity() {
    private val binding: ActivityMainBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
//        val s = LocationManager.getInstance().getLocationAll()[0]?.name_place
//        binding.tv.text = s
//
//        Toast.makeText(this, s, Toast.LENGTH_SHORT).show()
//        LocationManager.getInstance().addLocation(LocationTable(id = 1, latitude = "12", longitude = "100", name_place = "home1"))
//        LocationManager.getInstance().addLocation(LocationTable(id = 1, latitude = "12", longitude = "100", name_place = "home2"))
//        LocationManager.getInstance().addLocation(LocationTable(id = 1, latitude = "12", longitude = "100", name_place = "home3"))


    }

    override fun initListener() {
        binding.addLocation.setOnClickListener {
            NewLocationMapActivity.open(this)
        }
    }


    companion object{

//        private const val PAGE_ID         = "page_id"
//        private const val OPEN_REGISTER   = "open_register"
//        const val PAGE_HOME       = R.id.menu_home

        fun open(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
//            intent.putExtra(PAGE_ID, position)
//            intent.putExtra(OPEN_REGISTER, false)
            ActivityCompat.startActivity(
                context, intent, null
            )
        }
    }
}
