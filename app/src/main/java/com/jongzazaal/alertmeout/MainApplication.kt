package com.jongzazaal.alertmeout

import androidx.multidex.MultiDexApplication
import com.jongzazaal.alertmeout.manager.ContexterManager
import io.realm.Realm

class MainApplication(): MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        ContexterManager.getInstance().setApplicationContext(this)
        Realm.init(this)
    }

}